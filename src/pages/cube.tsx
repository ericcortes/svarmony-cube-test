import React from 'react';
import { Heading } from '@chakra-ui/react'

function CubePage() {

    return (
        <div>
            <Heading as='h1' size='4xl' noOfLines={1}>
                Just another Page
            </Heading>
        </div>
    );
}

export default CubePage;