import React from 'react';
import CubeObj from '../components/Layout/Cube'

function Home() {

    return (
        <div>
            <CubeObj />
        </div>
    );
}

export default Home;