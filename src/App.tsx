import { Routes, Route } from "react-router-dom"
import Home from "./pages/home"
import Cube from "./pages/cube"

function App() {

  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="cube" element={<Cube />} />
      </Routes>
    </div>
  );
}

export default App;
