import { useRef, useState } from 'react';
import * as THREE from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import ControlsColor from '../../UI/ControlsColor'
import ControlsRotate from '../../UI/ControlsRotate'
import XRLights from './Lights'
import XRObjects from './Objects'
import { HStack } from '@chakra-ui/react'

const XRScene = ({ props }: any) => {

    const [cubeProps, setCubeProps] = useState()
    const sceneRef = useRef(null);

    const { current: container } = sceneRef;

    // Three Elements
    const scene = new THREE.Scene();
    scene.background = new THREE.Color("#000000");

    const camera = new THREE.PerspectiveCamera(
        75,
        window.innerWidth / window.innerHeight,
        0.1,
        10
    );

    //camera.position.z = 5;
    camera.position.set(2, 2, 5);

    window.addEventListener('resize', onWindowResize, false);
    function onWindowResize() {
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();
        renderer.setSize(window.innerWidth, window.innerHeight);
    }

    //Renderer
    const renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild(renderer.domElement);

    // Controls
    const controls = new OrbitControls(camera, renderer.domElement);
    controls.enableZoom = false;
    controls.autoRotate = false;

    // Geometries
    const geometry = new THREE.BoxGeometry(1, 1, 1);
    const material = new THREE.MeshLambertMaterial({ color: 0xffad00 });
    const cube = new THREE.Mesh(geometry, material);
    scene.add(cube);

    // wireframe
    const cubeWireframe = new THREE.EdgesGeometry(cube.geometry); // or WireframeGeometry
    const wireframeMaterial = new THREE.LineBasicMaterial({ color: 0xd22730 });
    const wireframe = new THREE.LineSegments(cubeWireframe, wireframeMaterial);
    //cube.add(wireframe);

    camera.lookAt(cube.position);

    function animate() {
        requestAnimationFrame(animate);
        controls.update();
        renderer.render(scene, camera);
    }

    animate();

    return (
        <HStack
            pos="absolute"
        >
            {/* {props.children} */}
            {/* <XRObjects /> */}
            <XRLights scene={scene} />
            <ControlsColor cube={cube} />
            <ControlsRotate cube={cube} />
        </HStack>
    );
}

export default XRScene;

