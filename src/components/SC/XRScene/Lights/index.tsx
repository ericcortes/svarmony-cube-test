import * as THREE from "three";

const XRSceneLights = ({ props, scene }: any) => {
    console.log(scene)
    // Lights
    scene.add(new THREE.AmbientLight(0xffffff, 0.5));
    var light = new THREE.DirectionalLight(0xffffff, 1.1);
    light.position.set(-10, 10, 5);
    scene.add(light);

    return (
        null
    );
}

export default XRSceneLights;
