import * as THREE from "three";

const XRSceneObjects = ({ props, scene }: any) => {
    console.log(scene)
    // Geometries
    const geometry = new THREE.BoxGeometry(1, 1, 1);
    const material = new THREE.MeshLambertMaterial({ color: 0xffad00 });
    const cube = new THREE.Mesh(geometry, material);
    scene.add(cube);

    // wireframe
    const cubeWireframe = new THREE.EdgesGeometry(cube.geometry); // or WireframeGeometry
    const wireframeMaterial = new THREE.LineBasicMaterial({ color: 0xd22730 });
    const wireframe = new THREE.LineSegments(cubeWireframe, wireframeMaterial);
    cube.add(wireframe);

    return (
        null
    );
}

export default XRSceneObjects;
