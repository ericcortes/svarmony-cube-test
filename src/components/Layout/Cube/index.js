import { useEffect, useState } from 'react';
import XRScene from '../../SC/XRScene'

const Cube = ({ }) => {

  const [isAppRendered, setIsAppRendered] = useState(false);
  useEffect(() => {
    if (typeof window !== 'undefined') {
      setIsAppRendered(true);
    }
  }, [])
  console.log(isAppRendered)

  return (
    <>

      {isAppRendered && <XRScene />}
      {/* <XRScene>
          <XRSceneLights />
        </XRScene> */}

    </>
  );
}
export default Cube;
