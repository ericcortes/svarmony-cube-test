import { useRef, useState, useCallback } from 'react';
import { CirclePicker } from 'react-color';
import {
    Modal,
    ModalOverlay,
    ModalContent,
    Button,
    useDisclosure,
    Center,
} from '@chakra-ui/react'

const ControlsColor = ({ cube }: any) => {
    console.log(cube)

    //Change Color
    const [color, setColor] = useState('')
    const changeCubeColor = useCallback((color: any) => {
        setColor(color.hex);
        const newColor = color.hex.replace('#', '');
        console.log(newColor)
        cube.material.color.setHex(`0x${newColor}`)
        Number(`0x${color}`)
    }, []);
    console.log(color)

    const refModal = useRef(null)
    const { isOpen, onOpen, onClose } = useDisclosure()

    return (
        <>
            <Button
                id="btnChange"
                border='1px'
                borderColor='#39ff14'
                color='#39ff14'
                bg='transparent'
                _hover={{
                    background: "transparent",
                }}
                _focus={{
                    background: "transparent",
                }}
                ref={refModal}
                onClick={onOpen}
            >
                Change Color
            </Button>
            <Modal isOpen={isOpen} onClose={onClose} isCentered>
                <ModalOverlay />
                <ModalContent bg="transparent">
                    <Center>
                        <CirclePicker
                            circleSize={52}
                            width='450'
                            onChangeComplete={changeCubeColor}
                        />
                    </Center>
                </ModalContent>
            </Modal>
        </>
    );
}
export default ControlsColor;
