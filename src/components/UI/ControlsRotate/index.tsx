import { useEffect, useRef } from 'react';
import { Button, HStack } from '@chakra-ui/react'

const ControlsRotation = ({ cube }: any) => {

    const btnRotateXRef = useRef<HTMLButtonElement>(null)
    const btnRotateYRef = useRef<HTMLButtonElement>(null)

    useEffect(() => {
        console.log(btnRotateXRef)

        function UpdateRotation() {
            requestAnimationFrame(UpdateRotation)
            if (shouldRotateX) {
                cube.rotation.x += 0.1;
            }
            else if (shouldRotateY) {
                cube.rotation.y += 0.1;
            }
        }
        requestAnimationFrame(UpdateRotation)

        let shouldRotateX = false;
        btnRotateXRef.current?.addEventListener("mousedown", (event) => {
            shouldRotateX = true;
        });

        btnRotateXRef.current?.addEventListener("mouseup", (event) => {
            shouldRotateX = false;
        });

        let shouldRotateY = false;
        btnRotateYRef.current?.addEventListener("mousedown", (event) => {
            shouldRotateY = true;
        });

        btnRotateYRef.current?.addEventListener("mouseup", (event) => {
            shouldRotateY = false;
        });

        window.addEventListener("mouseup", function (event) {
            shouldRotateY = false;
            shouldRotateX = false;
        });
    })


    return (
        <HStack>
            <Button
                id="btnRotateY"
                ref={btnRotateYRef}
                border='1px'
                borderColor='#39ff14'
                color='#39ff14'
                bg='transparent'
                _hover={{
                    background: "transparent",
                }}
                _focus={{
                    background: "transparent",
                }}
            >
                Rotate Y
            </Button>
            <Button
                id="btnRotateX"
                ref={btnRotateXRef}
                border='1px'
                borderColor='#39ff14'
                color='#39ff14'
                bg='transparent'
                _hover={{
                    background: "transparent",
                }}
                _focus={{
                    background: "transparent",
                }}
            >
                Rotate X
            </Button>
        </HStack>
    );
}
export default ControlsRotation;
